package base.profile.memory;

import base.DataBaseService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

@Configuration
@Profile("memory")
public class InMemoryDatabaseConfig {

    @Bean
    public DataBaseService dataStorage() {
        return new DataBaseServiceImpl();
    }
}
