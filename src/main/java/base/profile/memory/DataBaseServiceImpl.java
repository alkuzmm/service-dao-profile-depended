package base.profile.memory;

import base.DataBaseService;

import java.util.LinkedHashSet;


public class DataBaseServiceImpl implements DataBaseService {

    private LinkedHashSet<String> hashSet = new LinkedHashSet<>();

    @Override
    public void save(String s) {
        hashSet.add(s);
    }

    @Override
    public String readAll() {
        StringBuilder stringBuilder = new StringBuilder();
        hashSet.forEach(s -> stringBuilder.append(s).append("\n"));
        return stringBuilder.toString();
    }
}
