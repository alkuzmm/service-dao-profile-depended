package base.profile.file;

import base.DataBaseService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

@Configuration
@Profile("file")
public class InFileDatabaseConfig {

    @Bean
    public DataBaseService dataStorage() {
        return new DataBaseServiceImpl();
    }
}
