package base.profile.file;

import base.DataBaseService;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.io.*;

public class DataBaseServiceImpl implements DataBaseService {

    private File file;
    private String path = "./src/main/resources/text.txt";

    public void setPath(String path) {
        this.path = path;
    }

    @PostConstruct
    public void init() throws Exception {

        file = new File(path);

        if (file.exists()) {
            if (!file.canWrite()) {
                throw new Exception("File can't be write ");
            }
        } else {
            if (!file.createNewFile()) {
                throw new Exception("File can't be created");
            }
        }
    }

    @Override
    public void save(String s) throws IOException {
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(file, true))) {
            writer.write(s);
            writer.write("\n");
        } catch (IOException ex) {
            throw new IOException("String can't be written");
        }
    }

    @Override
    public String readAll() throws IOException {
        try (BufferedReader reader = new BufferedReader(new FileReader(file))) {
            StringBuilder stringBuilder = new StringBuilder();
            String line = null;
            while ((line = reader.readLine()) != null) {
                stringBuilder.append(line).append("\n");
            }
            return stringBuilder.toString();
        } catch (IOException ex) {
            throw new IOException("File can't be read");
        }
    }

    @PreDestroy
    public void destroy() throws Exception {
        try {
            file.delete();
        } catch (Exception ex) {
            throw new IOException("File can't be deleted");
        }
    }
}
