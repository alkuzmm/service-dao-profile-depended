package base;

import java.io.FileNotFoundException;
import java.io.IOException;

public interface DataBaseService {

    void save(String s) throws IOException;

    String readAll() throws IOException;
}
