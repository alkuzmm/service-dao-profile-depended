package base;

import base.profile.file.InFileDatabaseConfig;
import base.profile.memory.InMemoryDatabaseConfig;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.GenericApplicationContext;


public class Main {
    public static void main(String[] args) throws Exception {

        GenericApplicationContext ctx = new AnnotationConfigApplicationContext(
                InMemoryDatabaseConfig.class,
                InFileDatabaseConfig.class,
                Service.class);

        Service service = ctx.getBean("service", Service.class);
        service.save("Line one");
        service.save("Line two");
        service.save("Line three");
        System.out.println(service.readAll());
        ctx.close();

    }
}
