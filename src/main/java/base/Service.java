package base;

import org.springframework.beans.factory.annotation.Autowired;

import java.io.IOException;

@org.springframework.stereotype.Service("service")
public class Service {

    private DataBaseService dataBaseService;

    @Autowired
    public Service(DataBaseService dataBaseService) {
        this.dataBaseService = dataBaseService;
    }

    public void save(String s) throws IOException {
        dataBaseService.save(s);
    }

    public String readAll() throws IOException {
        return dataBaseService.readAll();
    }
}
